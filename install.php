<?php

require_once 'utility.php';

$sql1 = "CREATE TABLE `users` (
  `name` varchar(50) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` text NOT NULL,
  `userhash` text NOT NULL,
  `email` text NOT NULL,
  `gender` enum('Male','Female') NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;";


$sql2 = "CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `postid` int(10) unsigned NOT NULL,
  `username` varchar(25) NOT NULL,
  `content` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38 ;";

$sql3 = "CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;";

if(!$connection=getPost::getConnection()){
	echo "Unable to connect to database, Please check login credentials";
	exit;
}
else{
	echo 'connected to database<br>';

if($connection->query($sql1)){
	echo 'users table created<br>';
	if($connection->query($sql2)){
		echo 'comments table created<br>';
		if($connection->query($sql3)){
			echo 'posts table created<br>';
			echo 'database setup completed<br>delete the config.php file';
			
		}
		else echo 'unable to create table posts<br>';
	}
	else echo 'unable to create table comments<br>';
}
else{
	echo 'unable to create table users check permissions';
}






}?>