<?php
require_once 'utility.php';
if(isset($_GET['postid'])){
	$postid=$_GET['postid'];
	$post=getPost::byid($postid);
	if($post){
?>
<?php if($username=validateCookie()){

if(isset($_POST['submit']) && $_POST['comment-content']){
// when user is logged in and posts a comment
	$comment=$_POST['comment-content'];
	$comm=new NewComment($post['id'], $username, $comment);
	$comm->dbPut();
	RedirectToURL("post.php?postid=".$post['id']);	
}
}?>
<!doctype html>
<html>
<head>
<title><?php echo $post['title']; ?></title>
<link rel='stylesheet' type='text/css' href='main.css'>

<script type="text/javascript">

function confirmSubmit()
{
var agree=confirm("Are you sure you wish to continue?");
if (agree)
	return true ;
else
	return false ;
}

</script>
</head>
<body>
<?php include 'nav.php';?>
<div id='container'>
<?php generateHeader();?>
<div id="post-page">
<!--if user logged in is author of post show option to edit or delete the post-->
<?php if($username === $post['username']){
echo "<div class='edit'><a href='edit.php?postid=".$post['id']."'>Edit</a></div>"
			.'<div class=\'edit\'><a onclick="return confirmSubmit()" href=\'edit.php?deleteid='.$post['id']."'>Delete</a></div>";
}?>
<!--post contents-->
<h1><?php echo $post['title']; ?></h1>
<div class="created">Posted on <?php echo $post['created']; ?></div>
<div class="content"><?php echo parseContent($post['content']);?></div>
<br><div class="author">By <a href="user.php?user=<?php echo $post['username'];?>"><?php echo $post['username'];?></a></div>
</div>
<!--end of post contents-->
<!-- comments-->
<ul id='comments'>
<?php 
$com=GetComment::byPostid($post['id']);
foreach ($com as $c){
	echo '<li><div class="author">'.$c['username'].' on '.$c['created']." said:</div><br>";
	if($username===$c['username']){
	echo '<div class="edit"><a onclick="return confirmSubmit()" href="edit.php?commdel='.$c['id'].'">Delete</a></div>';
	}
	echo '<span class="comment-content">'.str_replace("\r","<br>",$c['content']).'</span></li>';

}?>

</ul>
<!-- end of comments-->

<?php if($username){?>
<!--if user is logged in show form to post comment -->
<div id="postComment">
<form id='leaveAcomment' method='post'>
<h2>Leave A Comment</h2>
Logged in as <?php echo $username;?>..<br>
<textarea name='comment-content' id='comment-content'></textarea>
<input type="submit" name="submit" id="submit" value="Comment" class='button'></form></div>

<?php }
else{
// when user in not logged in show messege to login for commenting
echo "<a href='login.php'>Login</a> to comment..";
}?>
</div></body></html>
<?php }
	}?>