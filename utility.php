<?php
define('MYSQLUSER','pyareka');
define('MYSQLPASSWORD','breach10ck!)');
define('DATABASE','blog');
define('HOSTNAME','localhost');


//class to validate singup form values
class Validator{
	public $name;
	public $username;
	public $email;
	public $gender;
	public $password;
	public $verify;
	public $name_err;
	public $email_err;
	public $username_err;
	public $password_err;
	public $verify_err;
	//constructor to set different values of form
	public function __construct($name='',$username='',$email='',$gender='',$password='',$verify=''){
		$this->name=$name;
		$this->username=$username;
		$this->email=$email;
		$this->gender=$gender;
		$this->password=$password;
		$this->verify=$verify;
	}
	//validates email, checks whether email is already registered
	public function email(){
		$regexp="/^[^0-9][A-z0-9_]+([.][A-z0-9_])*[@][A-z0-9_]+([.][A-z0-9_])*[.][A-z]{2,4}$/";
		if(preg_match($regexp,$this->email)){
			$connection= @new mysqli(HOSTNAME,MYSQLUSER,MYSQLPASSWORD,DATABASE);
			if($connection->connect_errno){
				$this->email_err='Error connecting database';
				return False;
			}
			else{
				$query="SELECT email from `users` WHERE email='$this->email'";
				$res=$connection->query($query);
				if($res && $res->fetch_array()!=null){
					$this->email_err='Email already registered';
					return False;
				}
				else return $this->email;
			}
		}
		else{
			$this->email_err='Invalid Email';
			return False;
		}
	}
	//validates username, checks whether contains invalid string or already taken
	public function username(){
		$regexp="/^[a-z0-9]+[_]*[a-z0-9]+$/";
		if(preg_match($regexp,$this->username)){
			
				$connection= @new mysqli(HOSTNAME,MYSQLUSER,MYSQLPASSWORD,DATABASE);
				if($connection->connect_errno){
					$this->username_err='Error connecting database';
					return False;
				}
				else{
					$query="SELECT username from `users` WHERE username='$this->username'";
					$res=$connection->query($query);
					if($res && $res->fetch_array()!=null){
						$this->username_err='Username not available';
						return False;
					}
					else return $this->username;
				}
			
			
		}
		else{
			$this->username_err='Invalid Username';
			return False;
		}
	}
	//validates name
	public function name(){
		$regexp="/^[A-z]+[ A-z]*$/";
		if(preg_match($regexp,trim($this->name))){
			return trim($this->name);
		}
		else{
			$this->name_err='Invalid Name';
			return False;
		}
	}
	//validate password
	public function password(){
		
		if(strlen($this->password) > 3){
			
			return $this->password;
		}
		else{
			$this->password_err='Password Must be atleast 4 character';
			return False;
		}
	}
	//validate verify password
	public function verify(){
		if($this->verify === $this->password){
			return $this->verify;
			
		}
		else{
			$this->verify_err='Password do not match';
			return False;
		}
	}
	//validate gender
	public function gender(){
		if($this->gender=='Male' || $this->gender=='Female'){
			return $this->gender;
		}
		else{
			return False;
		}
	}
	//calls different methods on signup form's to validate  data
	public function validate(){
		$a=$this->email();
		$b=$this->gender();
		$c=$this->username();
		$d=$this->password();
		$e=$this->verify();
		$f=$this->name();
		if($a && $b && $c && $d && $e && $f){
			return True;
		}
		else return False;
	}

}



//to put new user's value in database

class dbPutUser{
	public $username;
	public $passHash;
	public $userHash;
	public $email;
	public $name;
	public $gender;
	
	public function __construct($name,$username,$password,$email,$gender){
		$this->username=$username;
		$this->email=$email;
		$this->gender=$gender;
		$this->name=trim($name);
		$salt=$this->genrateSalt();//generate 10 character salt
		$this->passHash=sha1($password.$salt).$salt;//add salt to password and convert into hash using sha1 and append salt to hash
		$this->userHash=sha1($this->username.$salt);//add salt to username and convert to hash using sha1
	}
	public function genrateSalt(){// to generate 10 character random salt
		$salt='';
		$key='abcdefghijklmnopqrstuvwjyz0123456789';
		for($i=1; $i<=10; $i++){
			$n=mt_rand(0,35);
			$salt.=$key[$n];
		}
		return $salt;
	}
	
	public function put(){//insert values to database and login user by setting cookies on success
		$connection= @new mysqli(HOSTNAME,MYSQLUSER,MYSQLPASSWORD,DATABASE);
		if($connection->connect_errno){
			
			return False;
		}
		else{
			$query="INSERT INTO `users` (`name`,`username`,`email`,`password`,`userhash`,`gender`)
					VALUES ('$this->name','$this->username','$this->email','$this->passHash','$this->userHash','$this->gender')";
			if(!$res=$connection->query($query)){
				echo 'query error';
				return False;
			}
			
			else{
				$p=substr($this->passHash, 0,$length=strlen($this->passHash)-10);
				setcookie('token',$this->userHash,0,$path='/');
				setcookie('becon',$p,0,$path='/');
				return True;
			}
		}
	}
}

function RedirectToURL($url)
{
	header("Location: $url");
	exit;
}

function safeValue($val){
	//returns empty string if $val contains quotes else return escaped value
	$regexp='/[\'\"]+/';
	if(preg_match($regexp,$val))
		return '';
	else
		return htmlentities($val);
}

//class to validate login of user
class Login{
	public $username;
	public $password;
	public $error;
	public function __construct($username,$password){//constructor to set values
		$this->username=$username;
		$this->password=$password;
	}
	public function validate(){
		$regexp="/^[a-z0-9]+[_]*[a-z0-9]+$/";
		if(preg_match($regexp,$this->username)){
				
			$connection= @new mysqli(HOSTNAME,MYSQLUSER,MYSQLPASSWORD,DATABASE);
			if($connection->connect_errno){
				$this->error='Error while Logging in';
				return False;
			}
			else{
				$query="SELECT `username`,`password`, `userhash` from `users` WHERE username='$this->username'";
				$res=$connection->query($query);
				$row=$res->fetch_array(MYSQLI_ASSOC);
				if($res && $row==null){
				//if no user exists with given username
					$this->error='Invalid Login';
					return False;
				}
				else {
				
					$p=substr($row['password'], 0,$length=strlen($row['password'])-10);//seperate password hash from hash
					
					$s=substr($row['password'], $length=strlen($row['password'])-10);//seperate salt from hash
					
					$c=sha1($this->password.$s);// generate hash of submitted password using the same salt
					
					if($c===$p){
					//if password hash matches set cookies and return username
						setcookie('token',$row['userhash'],0,$path='/');
						setcookie('becon',$c,0,$path='/');
						return $this->username;
					}
					else{
					//password do not match
						$this->error='Invalid Login';
						return False;
					}
				}
			}
				
				
		}
		else{
		//incomplete data
			$this->error='Invalid Login';
			return False;
		}
	}
	
	
}
//function to validate cookie
function validateCookie(){
	$regexp='/^[A-z0-9]+$/';
	if(isset($_COOKIE['token']) && isset($_COOKIE['becon'])){
	$token=$_COOKIE['token'];
	$becon=$_COOKIE['becon'];
	}
	else return False;
	if(preg_match($regexp,$token) && preg_match($regexp,$becon)){
	
		$connection= @new mysqli(HOSTNAME,MYSQLUSER,MYSQLPASSWORD,DATABASE);
		if($connection->connect_errno){
			
			return False;
		}
		else{
			$query="SELECT `username`,`password` from `users` WHERE userhash='$token';";
			$res=$connection->query($query);
			$row=$res->fetch_array(MYSQLI_ASSOC);
			if($res && $row==null){
				
				return False;
			}
			else {
				$p=substr($row['password'], 0,$length=strlen($row['password'])-10);				
				$c=$becon;
				if($c===$p){
					return $row['username'];
				}
				else{
					
					return False;
				}
			}
				
			}
		}
	
		else{
			return False;
		}
}
//to render the content of a post and safely allow different html tags
function parseContent($html){
	$html = str_replace("\r", "<br>", $html);
	$html = preg_replace("!\n\n+!", "\n", $html);
		
	/* Line break */
	$html = preg_replace('!&lt;br&gt;!m',
			'<br>',
			$html);

	/* Emphasized Text */
	$html = preg_replace('!&lt;em&gt;(.*?)&lt;/em&gt;!m',
			'<em>$1</em>',
			$html);
	
	$html = preg_replace('!&amp;!m',
			'&',
			$html);
	
	/* Blockquotes */
	$html = preg_replace('!&lt;blockquote&gt;(.*?)&lt;/blockquote&gt;!m',
			'<blockquote><p>$1</p></blockquote>',
			$html);
	
	/* Paragraph */
	$html = preg_replace('!&lt;p&gt;(.*?)&lt;/p&gt;!m',
			'<p>$1</p>',
			$html);
	
	/* Code */
	$html = preg_replace('!&lt;code&gt;(.*?)&lt;/code&gt;!m',
			'<code>$1</code>',
			$html);
			
	/*Strong */
	$html = preg_replace('!&lt;strong&gt;(.*?)&lt;/strong&gt;!m',
			'<strong>$1</strong>',
			$html);
	
	/*Heading 1 */
	$html = preg_replace('!&lt;h1&gt;(.*?)&lt;/h1&gt;!m',
			'<h2>$1</h2>',
			$html);
	/*Heading 2 */
	$html = preg_replace('!&lt;h2&gt;(.*?)&lt;/h2&gt;!m',
			'<h3>$1</h3>',
			$html);
			
	/*list */
	$html = preg_replace('!&lt;li&gt;(.*?)&lt;/li&gt;!m',
			'<li>$1</li>',
			$html);
			
	/*ul */
	$html = preg_replace('!&lt;ul&gt;(.*?)&lt;/ul&gt;!m',
			'<ul>$1</ul>',
			$html);
			
	/*ol */
	$html = preg_replace('!&lt;ol&gt;(.*?)&lt;/ol&gt;!m',
			'<ol>$1</ol>',
			$html);

			
	/* Italic */
	$html = preg_replace('!&lt;i&gt;(.*?)&lt;/i&gt;!m',
			'<i>$1</i>',
			$html);
	
	/* Bold */
	$html = preg_replace('!&lt;b&gt;(.*?)&lt;/b&gt;!m',
			'<b>$1</b>',
			$html);
	
	/* Links */
	$html = preg_replace('!&lt;a +href=&quot;((?:ht|f)tps?://.*?)&quot;(?: +title=&quot;(.*?)&quot;)? *&gt;(.*?)&lt;/a&gt;!m',
			'<a href="$1" title="$2">$3</a>',
			$html);
	/* Images */
	$html = preg_replace('!&lt;img +src=&quot;((?:ht|f)tps?://.*?)&quot;(?: +alt=&quot;(.*?)&quot;)? *&gt;!m',
			'<img src="$1" alt="$2">',
			$html);
	return $html;
	
}

// to add new post
class NewPost{
	public $title;
	public $content;
	public $username;
	public $error;
	//contructor to set variuos values
	public function __construct($title, $content, $username){
		$this->title=htmlentities($title);
		$this->content=htmlentities($content);
		$this->username=$username;
	}
	//put values in database
	public function dbPut(){
		$connection= new mysqli(HOSTNAME,MYSQLUSER,MYSQLPASSWORD,DATABASE);
		if($connection->connect_errno){
			
			$this->error='Error while Logging into database';
			return False;
		}
		else{
			$this->title=$connection->real_escape_string($this->title); //escape title
			$this->content=$connection->real_escape_string($this->content); //escape content
			$query="INSERT INTO `posts` (`title`,`content`,`username`)
					VALUES ('$this->title','$this->content','$this->username')";
			$res=$connection->query($query);
			
			if(!$res){
				$this->error='Error Connecting Database';
				return False;
			}
			else{
				$post=array();
				$post['title']=$this->title;
				$post['content']=$this->content;
				$post['username']=$this->username;
				return $post;
			}
		}
	}
}
//to get posts
class getPost{
	public static $error;
	
	public static function getConnection(){
		$connection= new mysqli(HOSTNAME,MYSQLUSER,MYSQLPASSWORD,DATABASE);
		if($connection->connect_errno){
				
			self::$error='Error while Logging into database';
			return False;
		}
		else{
			return $connection;
		}
	}
	//get recent 10 posts ordered by date of creation
	public static function top($n){
		$query="SELECT * FROM `posts` ORDER BY `created` desc LIMIT $n";
		$connection=self::getConnection();
		$res=$connection->query($query);
		
		if(!$res){
			self::$error="Error while retriving from database";
			return False;
		}
		else{
			$top=array();
			while($result=$res->fetch_array(MYSQL_ASSOC)){
				
				$top[]=$result;
			}
			return $top;
		}
	}
	//get title of all posts ordered by date of creation
	public static function archive(){
		$query="SELECT `id`, `title` FROM `posts` ORDER BY `created` desc";
		$connection=self::getConnection();
		$res=$connection->query($query);
	
		if(!$res){
			self::$error="Error while retriving from database";
			return False;
		}
		else{
			$top=array();
			while($result=$res->fetch_array(MYSQL_ASSOC)){
	
				$top[]=$result;
			}
			return $top;
		}
	}
	//get single post by id
	public static function byid($postid){
		$postid=(int)$postid;
		$query="SELECT * FROM `posts` WHERE `id`='$postid'";
		$connection=self::getConnection();
		$res=$connection->query($query);
		
		if(!$res){
			self::$error="Error while retriving from database";
			return False;
		}
		else{
			$result=$res->fetch_array(MYSQL_ASSOC);
			return $result;
		}
	}
	// get id of recently added post
	public static function recentid(){
		$query="SELECT id FROM `posts` ORDER BY `created` DESC LIMIT 1";
		$connection=self::getConnection();
		$res=$connection->query($query);
		if($res){
			$result=$res->fetch_array(MYSQL_ASSOC);
			return $result['id'];
		}
	}
	//update a old post
	public static function update($postid,$title, $content){
		$connection=self::getConnection();
		$postid=(int)$postid;
		$title=htmlentities($title,ENT_QUOTES);
		$content=htmlentities($content,ENT_QUOTES);
		if($postid && $title && $content){
		$query="UPDATE `posts` SET `title`='$title', `content`='$content' where id='$postid'";
		if($connection->query($query)){
			return $postid;
		}
	}
	}
}
//to add comment
class NewComment{
	public $postid;
	public $username;
	public $content;
	public $error;
	//constructor to set different values
	public function __construct($postid,$username,$comment){
		$this->postid=(int)$postid;
		$this->username=$username;
		$this->content=$comment;
	}
	//add comment to database
	public function dbPut(){
		if($connection=getPost::getConnection()){
			$this->content=$connection->real_escape_string($this->content);//escape comment
			$query="INSERT INTO `comments` (`postid`,`username`,`content`)
					VALUES ('$this->postid','$this->username','$this->content')";
			$result=$connection->query($query);
			if($result){
				return True;
			}
			else{
				return False;
			}
		}
		else{
			$this->error="db connectivity prob";
		}
	}
}
//to get comments
class GetComment{

	public static function byPostid($postid){ //get all comments on a post
		$postid=(int)$postid;// convert postid to int
		if($connection=getPost::getConnection()){
			$query="SELECT * FROM `comments` WHERE postid='$postid' ORDER BY created";
			$res=$connection->query($query);
			if($res){
			$top=array();
			while($result=$res->fetch_array(MYSQL_ASSOC)){
			
				$top[]=$result;
			}
			return $top;
		}
		}
	}
	public static function top($n){ //get recently posted n comments
		$n=(int)$n;//convert n to int
		if($connection=getPost::getConnection()){
			$query="SELECT * FROM `comments` ORDER BY created desc LIMIT $n";
			$res=$connection->query($query);
			if($res){
				$top=array();
				while($result=$res->fetch_array(MYSQL_ASSOC)){
						
					$top[]=$result;
				}
				return $top;
		}
	}
}

	public static function byid($postid){//get a comment by its id
		$postid=(int)$postid;//convert to int
		if($connection=getPost::getConnection()){
			$query="SELECT * FROM `comments` WHERE id='$postid' ORDER BY created";
			$res=$connection->query($query);
			if($res){
				
				return $res->fetch_array(MYSQL_ASSOC);
			}
		}
	}
}

//to delete comments, posts
class Delete{
	public static function post($n){//delete post and comments on it by postid
		$n=(int)$n;
		if($connection=getPost::getConnection()){
			$query1="DELETE FROM `posts` WHERE id='$n'";
			$query2="DELETE FROM `comments` WHERE postid='$n'";
			$res=$connection->query($query1);
			$res2=$connection->query($query2);
			if($res && $res2){
				return True;
			}
			else{
				return False;
			}
		}
		
	}
	public static function comment($n){//delete comment by id
		$n=(int)$n;
		if($connection=getPost::getConnection()){
			$query="DELETE FROM `comments` WHERE `id`='$n'";
			$res=$connection->query($query);
			if($res){
				return True;
			}
			else{
				
				return False;
			}
			
		}
	}
}
//get user details
class getUser{
	public static function byusername($username){ //by username
		$connection=getPost::getConnection();
		$username=$connection->real_escape_string($username);
		$query="SELECT * FROM `users` where `username`='$username'";
		if($res=$connection->query($query)){
			return $res->fetch_array(MYSQLI_ASSOC);
		}
	}
}
//header images and title of template
function generateHeader($n=1){
	
	if($n===1){
		echo "<div id='head'><img src=\"images/head/".mt_rand(17, 22).".png\">";
		echo "<br><BR> Ajay's Diary</div>";
	}
	elseif($n===2){
		echo "<img src=\"images/head/".mt_rand(1,8).".jpg\"id='head'>";
	}
	else echo "<img src=\"images/head/".mt_rand(9, 16).".jpg\"id='head'>";
}


?>
