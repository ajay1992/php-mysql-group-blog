--------------------------------------------------------------------------------------------------------
## simple group blog in php
	report any bug or issues at ajay@meajay.in
	http://www.meajay.in
--------------------------------------------------------------------------------------------------------
## INSTALLATION
	1. open utility.php and change the following lines with your database credentials
	================================================================================================
	define('MYSQLUSER','root');//replace root with your db username
	define('MYSQLPASSWORD','password');// replace password with your db password
	define('DATABASE','blog');// replace blog with database name
	define('HOSTNAME','localhost');// replace localhost with your db host generally it is localhost
	================================================================================================

	2. upload all the contents of blog folder to desired directory
	3. run install.php
	4. if received message of installation successfull then installation is sucessfull
	5. delete install.php
--------------------------------------------------------------------------------------------------------
## FILES
	#index.php
	main page
	
	#main.css
	css style
	
	#install.php
	to create different tables in database used by blog
	
	#edit.php
	to edit existing post
	
	#login.php
	login page
	
	#logout.php
	to logout user
	
	#nav.php
	navigation bar
	
	#newpost.php
	page to create new post
	
	#post.php
	page to show individual post
	
	#signup.php
	signup page
	
	#user.php
	page to show user information
	
	#utility.php
	contains different functions and classes used throught
---------------------------------------------------------------------------------------------------------

## LICENSE
	This program is free software published under the terms of the GNU [General Public License]
	(http://www.gnu.org/licenses/gpl.html).
	You can freely use it on commercial or non-commercial websites.
---------------------------------------------------------------------------------------------------------
