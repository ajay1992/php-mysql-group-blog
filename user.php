<?php
require_once 'utility.php';
$username=validateCookie();
if($username){
?>
<!doctype html>
<html>
<head>
<title>User</title>
<link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
<?php include 'nav.php'; ?>
<div id="container">
<?php generateHeader();?>
<div id="user">
<img src="images/avatar.jpg">
<?php
if(isset($_GET['user'])){
if($username===htmlentities($_GET['user'])){
	RedirectToURL('user.php');
}
else{
	$user=getUser::byusername($_GET['user']);
	if($user){
		
		echo "<h2>Username:".$user['username']."</h2>";
		echo "<h2>Name:".$user['name']."</h2>";
		echo "<h2>Gender:".$user['gender']."</h2>";
		echo "<h2>Registered:".$user['date']."</h2>";

	 }
}
}
else{
	$user=getUser::byusername($username);
	if($user){
	
		echo "<h2>Username: ".$user['username']."</h2>";
		echo "<h2>Name: ".$user['name']."</h2>";
		echo "<h2>Gender: ".$user['gender']."</h2>";
		echo "<h2>Registered: ".$user['date']."</h2>";
		echo "<h2>Email: ".$user['email']."</h2>";
	}
}
}
else{
RedirectToURL('login.php');
}?>
</div>
</div>
</body>
</html>