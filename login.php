<?php
require_once 'utility.php';
$username=$password='';
$login_err='';
if(isset($_POST['submit'])){
	$username=$_POST['username'];
	$password=$_POST['password'];
	$val=new Login($username,$password);
	if($val->validate()){
	//login sucessfull
		RedirectToURL(dirname($_SERVER['PHP_SELF']));
	}
	else{
	//login failed
		$login_err=$val->error;
	}
}

?>
<!doctype html>
<html>
<head>
<title>Login</title>
<style type='text/css'>
.error{
color:red;
}
</style>
<link rel='stylesheet' type='text/css' href='main.css'>
</head>
<body>
<?php include 'nav.php';?>
<div id='container'>
<?php generateHeader();?>
<form method='post' id='login'>
<div id="login-ti">Login</div>
<table><tr><td>
<label for='username'>Username</label></td><td>
<input type='text' name='username' id='useraname' value="<?php echo safeValue($username); ?>"></td></tr>
<tr><td><label for='password'>Password</label></td><td>
<input type='password' name='password' id='password' value="<?php echo safeValue($password); ?>"></td></tr>
<tr><td><input type='submit' name='submit' id='submit' value='Login' class='button'></td>
<td><?php echo '<div class="error">'.$login_err."</div>"; ?></td>
</tr></table><br>
Not a member yet? <a href='signup.php'>Signup</a>
</form></div></body></html>