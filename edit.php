<?php
require_once 'utility.php';
$username=validateCookie();
if(isset($_GET['deleteid'])){
	$post= getPost::byid($_GET['deleteid']);
	if($username && $username===$post['username']){
		if(Delete::post($_GET['deleteid'])){
			RedirectToURL(dirname($_SERVER['PHP_SELF']));
		}
		else{
			RedirectToURL(dirname($_SERVER['PHP_SELF']));
		}
	}
	else{
		RedirectToURL(dirname($_SERVER['PHP_SELF']));
	}
}
elseif (isset($_GET['commdel'])) {
	$comm=GetComment::byid($_GET['commdel']);
	if($username && $username===$comm['username']){
		if(Delete::comment($_GET['commdel'])){
			RedirectToURL('post.php?postid='.$comm['postid']);
		}
		else{
			
			RedirectToURL(dirname($_SERVER['PHP_SELF']));
		}
	}
	else{
		
		RedirectToURL(dirname($_SERVER['PHP_SELF']));
	}
}
elseif (isset($_GET['postid'])){
	$err='';
	$post=getPost::byid($_GET['postid']);
	$title=$post['title'];
	$content=$post['content'];
	if($username != $post['username']){
		exit;
	}
	if(isset($_POST['submit'])){
		$title=$_POST['title'];
		$content=$_POST['content'];
		$postid=$_GET['postid'];
		if($postid && $title && $content){
			if(getPost::update($postid, $title, $content)){
				RedirectToURL("post.php?postid=".$postid);
			}
		}
	
	
	}
}
else{
	RedirectToURL(dirname($_SERVER['PHP_SELF']));
}
?>
<?php if(isset($_GET['postid'])){?>
<!doctype html>
<html>
<head>
<title>Edit Post</title>
<link rel="stylesheet" type='text/css' href="main.css">
</head>
<body>
<?php include 'nav.php';?>
<?php if($username===$post['username']){?>
<div id="container">
<?php generateHeader();?>
<form id="newpost" method="post" action="?postid=<?php echo $_GET['postid'];?>">
<div id="newpost-ti">Edit Post</div>
<input type='text' name='title' id='title' value="<?php echo $title; ?>">
<br><br>
<textarea name="content" id="content"><?php echo $content; ?></textarea>
<input type="submit" value="Post" name='submit' id='submit' class='button'><?php echo "<span class='error'>$err</span>"; ?>
</form>
<?php }
else{ 
echo'<h1><br><br><br>Not authorized</h1>';
}?>
</div>
</body>
</html>
<?php }?>