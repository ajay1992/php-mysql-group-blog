<?php
$name=$username=$gender=$email=$password=$verify='';
require_once 'utility.php';
$name_err=$password_err=$verify_err=$username_err=$email_err='';

if(isset($_POST['submit'])){
//when user submits signup form
	$name=$_POST['name'];
	$username=$_POST['username'];
	$gender=$_POST['gender'];
	$email=$_POST['email'];
	$password=$_POST['password'];
	$verify=$_POST['verify'];

	$values=new Validator($name, $username, $email, $gender, $password, $verify);
	$flag=$values->validate(); // validate values
	//set different errors
	$name_err=$values->name_err;
	$password_err=$values->password_err;
	$verify_err=$values->verify_err;
	$email_err=$values->email_err;
	$username_err=$values->username_err;
	if($flag){
	//when values are valid
		$user=new dbPutUser($name, $username, $password, $email, $gender);//prepair data
		if($user->put()){//add to database
			RedirectToURL(dirname($_SERVER['PHP_SELF']));//redirect to home
			
		}
		else echo 'Registration Failed';
	}
	
	
}
?>
<!doctype html>
<html>
<head>
<title>Signup</title>
<link rel='stylesheet' type='text/css' href='main.css'>
<style type='text/css'>
body{
font-family:Helvetica;
font-size:14px;
}
.error{
color:red;
}


</style>
</head>
<body>
<?php include 'nav.php';?>
<div id='container'>
<?php generateHeader();?>
<form method='post' id="register">
<div id='reg'>Signup</div>
<table><tr><td>
<label for='name'>Name</label></td><td>
<input type='text' name='name' id='name' value="<?php echo safeValue($name); ?>"></td><td><?php echo "<span class='error'>".$name_err."</span>"; ?></td></tr>
<tr><td><label for= 'username'>Username</label></td><td>
<input type='text' name='username' id='username' value="<?php echo safeValue($username);?>"></td><td><?php echo "<span class='error'>".$username_err."</span>"; ?></td></tr>
<tr><td><label for= 'gender'>Gender</label></td><td>
<select name='gender' id='gender'>
<option>Male</option>
<option>Female</option>
</select><br></td></tr>
<tr><td><label for ='email'>Email</label></td><td>
<input type='text' name='email' id='email' value="<?php echo safeValue($email); ?>"></td><td><?php echo "<span class='error'>".$email_err."</span>"; ?></td></tr>
<tr><td><label for='password'>Password</label></td><td>
<input type='password' name='password' id='password' value="<?php echo safeValue($password);?>"></td><td><?php echo "<span class='error'>".$password_err."</span>"; ?></td></tr>
<tr><td><label for= 'verify'>Verify Password</label></td><td>
<input type='password' name='verify' id='verify' value="<?php echo safeValue($verify); ?>"></td><td><?php echo "<span class='error'>".$verify_err."</span>"; ?></td></tr>
</table>
<input type='submit' value='Submit' name='submit' id='submit' class='button'>
</form>
</div>
</body></html>

