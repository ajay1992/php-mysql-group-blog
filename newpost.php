<?php
require_once 'utility.php';

$username=validateCookie();//check whether user is logged in or not
$err='';
$title='Title';
$content='Content';
?>
<?php if(isset($_POST['submit']) && $username){
//when data is submitted and user is logged in
$title=$_POST['title'];
$content=$_POST['content'];
if($title && $content){

$post=new NewPost($title, $content, $username);
if($res=$post->dbPut()/*add post to database*/){
	RedirectToURL('post.php?postid='.getPost::recentid());	
}
else{ //unable to add content to database
$err="Database Connectivity Prob";
}

}
else{ //blank title or content
$err='Both fields are required';
}
}?>
<!doctype html>
<html>
<head>
<title>Create New Post</title>
<link rel="stylesheet" type='text/css' href="main.css">
<script type="text/javascript">
 function clearMe(formfield){
  if (formfield.defaultValue==formfield.value)
   formfield.value = ""
 }
 </script> 
</head>
<body>
<?php include 'nav.php';?>
<?php if($username){?>
<!--when user is logged in-->
<div id="container">
<?php generateHeader();?>
<form id="newpost" method="post">
<div id="newpost-ti">Create New Post</div>
<input type='text' name='title' id='title' value="<?php echo htmlentities($title)?>" onfocus='clearMe(this)'>
<br><br>
<textarea name="content" id="content" onfocus='clearMe(this)'><?php echo htmlentities($content); ?></textarea>
<input type="submit" value="Post" name='submit' id='submit' class='button'><?php echo "<span class='error'>$err</span>"; ?>
</form>
<?php }
else{ 
// when user is not logged in
echo'<h1><br><br><br>Not authorized</h1>';
}?>
</div>
</body>
</html>

