<?php
require_once 'utility.php';

?>
<!doctype html>
<html>
<head>
<title>hahaha</title>
<link rel="stylesheet" type='text/css' href='main.css'>
</head>
<body>
<!--navigation bar-->
<?php include 'nav.php';?>
<div id='container'>
<!--head image and title-->
<?php generateHeader(1);?>
<!--Recent comment Sidebar-->
<div id="recentComments">
<h2>Recent Comments..</h2>
<ul>
<?php 
$recent=GetComment::top(5);
if($recent){
foreach ($recent as $comm){
	
	echo '<li><a href="post.php?postid='.$comm['postid'].'">'.$comm['username'].": ".substr($comm['content'], 0,60).'...</a></li>';
}
}
?>
</ul></div>
<!--end of recent commenet -->
<!--archive sidebar -->
<div id='archive'>
<h2>Archive</h2>
<ul>
<?php 
$arch=GetPost::archive();
if($arch){
foreach ($arch as $a){

	echo '<li><a href="post.php?postid='.$a['id'].'">'.$a['title'].'...</a></li>';
}
}
?>



</ul></div>
<!--end of archive -->
<!-- recent 10 posts -->
<div id="posts">
<?php 
$top=getPost::top(10);

foreach ($top as &$post) {
?>
<div class="top10">
<h1><a href="post.php?postid=<?php echo $post['id'];?>"><?php echo $post['title']; ?></a></h1>
<div class="created">Posted on <?php echo $post['created']; ?></div>
<div class="content"><?php echo parseContent($post['content']);?></div>
<div class="author">By <a href="user.php?user=<?php echo $post['username'];?>"><?php echo $post['username'];?></a></div>

</div>
<?php } ?>
</div>
<!-- end of posts-->


</div></body></html>



